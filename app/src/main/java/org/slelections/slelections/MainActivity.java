package org.slelections.slelections;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.philjay.valuebar.ValueBar;

import org.slelections.slelections.neo4j.Neo4jAPIManager;
import org.slelections.slelections.neo4j.Neo4jProvider;
import org.slelections.slelections.neo4j.RequestBody;
import org.slelections.slelections.neo4j.ResponseBody;
import org.slelections.slelections.neo4j.Statement;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    //TextView Animations//
    static final Animation fadeIn = new AlphaAnimation(0, 1);

    static {
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1000);
    }

    static final Animation fadeOut = new AlphaAnimation(1, 0);

    static {
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000);
    }

    final ButterKnife.Setter<TextView, ElectionSummary> UPDATE_ELECTION_DATA = new ButterKnife.Setter<TextView, ElectionSummary>() {
        @Override
        public void set(TextView view, ElectionSummary data, int index) {

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

            String s;
            view.startAnimation(fadeOut);
            switch (index) {
                case 0:
                    s = String.format("%.2f", data.getLeftPercentage());
                    view.setText(s + "%");
                    break;
                case 1:
                    s = String.format("%.2f", data.getRightPercentage());
                    view.setText(s + "%");
                    break;
                case 2:
                    s = String.format("%.2f", data.getOthersPercentage());
                    view.setText("Others: " + s + "%");
                    break;
                case 3:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getLeftVotes()));
                    break;
                case 4:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getRightVotes()));
                    break;
                case 5:
                    if (dpWidth < 360.0) {
                        String li1 = data.getLeftName().split("(?<=[\\S])[\\S]*\\s*")[0];
                        String li2 = data.getLeftName().split("(?<=[\\S])[\\S]*\\s*")[1];
                        view.setText(li1 + li2);
                    } else {
                        String lname = data.getLeftName().split(" ")[1];
                        view.setText(lname.toUpperCase());
                    }
                    break;
                case 6:
                    if (dpWidth < 360.0) {
                        String ri1 = data.getRightName().split("(?<=[\\S])[\\S]*\\s*")[0];
                        String ri2 = data.getRightName().split("(?<=[\\S])[\\S]*\\s*")[1];
                        view.setText(ri1 + ri2);
                    } else {
                        String rname = data.getRightName().split(" ")[1];
                        view.setText(rname.toUpperCase());
                    }
                    break;
                case 7:
                    view.setText("\u25B2");
                    break;
                case 8:
                    view.setText("All Island Summary");
                default:
                    break;
            }
            view.startAnimation(fadeIn);
        }
    };
    final ButterKnife.Setter<TextView, ElectionMetaSummary> UPDATE_ELECTION_META_DATA = new ButterKnife.Setter<TextView, ElectionMetaSummary>() {
        @Override
        public void set(TextView view, ElectionMetaSummary data, int index) {

            String s;

            view.startAnimation(fadeOut);
            switch (index) {
                case 0:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getValidVotes()));
                    break;
                case 1:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getRejectedVotes()));
                    break;
                case 2:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getTotalPolled()));
                    break;
                case 3:
                    view.setText(NumberFormat.getNumberInstance(Locale.US).format(data.getRegisElectors()));
                    break;
                case 4:
                    s = String.format("%.2f", data.getValidVotesPercentage());
                    view.setText(s + "%");
                    break;
                case 5:
                    s = String.format("%.2f", data.getRejectedVotesPercentage());
                    view.setText(s + "%");
                    break;
                case 6:
                    s = String.format("%.2f", data.getTotalPolledPercentage());
                    view.setText(s + "%");
                    break;
                case 7:
                    view.setText("Valid Votes");
                    break;
                case 8:
                    view.setText("Rejected Votes");
                    break;
                case 9:
                    view.setText("Total Polled");
                    break;
                case 10:
                    view.setText("Regis. Electors");
                    break;
                default:
                    //Date date = new Date(System.currentTimeMillis());
                    Date date = new Date(1420893408966l);
                    DateFormat format = new SimpleDateFormat("MMMM dd, yyyy 'at' kk:mm 'SLST'");
                    format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                    String timestamp = format.format(date);
                    view.setText("final sync with www.slelections.gov.lk\n" + timestamp);
                    break;

            }
            view.startAnimation(fadeIn);
        }
    };
    final static Statement cypherSummery = new Statement("match (e:Election {year:2015})-[:HAS_RESULT]-(r:Result)-[p:POLLED]-(c:Candidate) return c.name as name, sum(p.votes)/toFloat(sum(r.validVotes))*100 as percentage, sum(p.votes) as votes order by c.name");
    final static Statement cypherReporting = new Statement("match (e:Election {year:2015})-[:HAS_RESULT]-(r:Result)-[:HAS_RESULT]-(d:Division) match (all:Division) return count(distinct r)/toFloat(count(distinct all))*100 as percentage");
    final static Statement cypherResultSummary = new Statement("match (e:Election {year:2015})-[HAS_RESULT]-(r:Result) with sum(r.validVotes) as vv, sum(r.rejectedVotes) as rv, sum(r.electors) as re return vv, rv, vv+rv as tp, re");
    final static Statement cypherResultSummaryPercentages = new Statement("match (e:Election {year:2015})-[HAS_RESULT]-(r:Result) with sum(r.validVotes) as vv, sum(r.rejectedVotes) as rv, sum(r.electors) as re return vv/toFloat(re)*100 as vv_p, rv/toFloat(re)*100 as rv_p, (vv+rv)/toFloat(re)*100 as tv_p");
    final ButterKnife.Action<ValueBar> INIT_VALUE_BAR = new ButterKnife.Action<ValueBar>() {
        @Override
        public void apply(ValueBar bar, int index) {
            bar.setMinMax(0, 100);
            bar.setDrawValueText(false);
            bar.setDrawBorder(false);
            bar.setDrawMinMaxText(false);
            bar.setTouchEnabled(false);
            switch (index) {
                case 0:
                    bar.setBackgroundColor(getResources().getColor(R.color.election_widget_right));
                    bar.setColor(getResources().getColor(R.color.primary_light));
                    bar.setValue(100f);
                    break;
                case 1:
                    bar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    bar.setColor(getResources().getColor(R.color.election_widget_left));
                    bar.setValue(0f);
                    break;
                default:
                    break;
            }
        }
    };
    // TODO: Investigte animating bars
    final ButterKnife.Setter<ValueBar, Pair<Float, Float>> UPDATE_VALUE_BAR = new ButterKnife.Setter<ValueBar, Pair<Float, Float>>() {
        @Override
        public void set(ValueBar bar, Pair<Float, Float> value, int index) {
            switch (index) {
                case 0:
                    if (bar.getValue() < (100 - value.second)) {
                        bar.animateDown(100 - value.second, 1000);
                    } else {
                        bar.animateUp(100 - value.second, 1000);
                    }
                    break;
                case 1:
                    if (bar.getValue() < value.first) {
                        bar.animateUp(value.first, 1000);
                    } else {
                        bar.animateDown(value.first, 1000);
                    }
                    break;
                default:
                    break;
            }

        }
    };
    //VallueBar Inits
    @InjectViews({R.id.rightValueBar, R.id.leftValueBar})
    List<ValueBar> valueBars;
    //Election widget inits
    @InjectViews({R.id.leftPercentage, R.id.rightPercentage, R.id.others, R.id.leftVotes, R.id.rightVotes, R.id.leftName, R.id.rightName, R.id.center, R.id.reporting})
    List<TextView> electionWidget;
    //Result summary absolute
    @InjectViews({R.id.absoluteValidVotes, R.id.absoluteRejectedVotes, R.id.absoluteTotalPolled, R.id.regisElectors,
            R.id.percentageValidVotes, R.id.percentageRejectedVotes, R.id.percentageTotalPolled,
            R.id.validVotesLabel, R.id.rejectedVotesLabel, R.id.totalPolledLabel, R.id.regisElectorsLabel,
            R.id.lastUpdated})
    List<TextView> electionMetaWidget;
    ListView resultListView;
    //ResultCursorAdapter adapter;
    DistrictCursorAdapter adapter;

    @InjectView(R.id.emptyImageView)
    ImageView emptyImageView;
    @InjectView(R.id.emptyTextView)
    TextView emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Sri Lanka Elections");

        resultListView = (ListView) findViewById(R.id.resultListView);
        View header = getLayoutInflater().inflate(R.layout.listview_header, null);
        View footer = getLayoutInflater().inflate(R.layout.listview_footer, null);
        LinearLayout emptyView = (LinearLayout) findViewById(R.id.emptyView);

        resultListView.setEmptyView(emptyView);
        resultListView.addHeaderView(header);
        resultListView.addFooterView(footer);

        ButterKnife.inject(this);
        emptyImageView.setVisibility(View.VISIBLE);
        emptyTextView.setText(Html.fromHtml(getString(R.string.empty_placeholder)));

        updateListViewHeader(true);

        //adapter = new ResultCursorAdapter(MainActivity.this, R.layout.listview_item_division, null, new String[]{}, new int[]{}, 0);
        adapter = new DistrictCursorAdapter(MainActivity.this, R.layout.listview_item_district, null, new String[]{}, new int[]{}, 0);
        resultListView.setAdapter(adapter);

        getLoaderManager().initLoader(0, null, MainActivity.this);
    }

    private void updateListViewHeader(final boolean init) {

        //TODO: Set Empty Message
        emptyImageView.setVisibility(View.VISIBLE);
        emptyTextView.setText(Html.fromHtml(getString(R.string.empty_placeholder)));

        List<Statement> statements = new ArrayList<>();
        statements.add(cypherSummery);
        statements.add(cypherReporting);
        statements.add(cypherResultSummary);
        statements.add(cypherResultSummaryPercentages);

        RequestBody body = new RequestBody(statements);

        Neo4jAPIManager.getRestApi().runCypher(body, new Callback<ResponseBody>() {
            @Override
            public void success(ResponseBody responseBody, Response response) {

                if (responseBody.getResults().get(0).getData().size() > 0) {


                    String leftName = (String) responseBody.getResults().get(0).getData().get(0).getRow().get(0);
                    String rightName = (String) responseBody.getResults().get(0).getData().get(1).getRow().get(0);

                    float leftPercentage = (float) ((double) responseBody.getResults().get(0).getData().get(0).getRow().get(1));
                    float rightPercentage = (float) ((double) responseBody.getResults().get(0).getData().get(1).getRow().get(1));

                    float leftVotes = (float) ((double) responseBody.getResults().get(0).getData().get(0).getRow().get(2));
                    float rightVotes = (float) ((double) responseBody.getResults().get(0).getData().get(1).getRow().get(2));

                    float reportingPercentage = (float) ((double) responseBody.getResults().get(1).getData().get(0).getRow().get(0));

                    ElectionSummary data = new ElectionSummary(leftName, rightName, leftPercentage, rightPercentage, leftVotes, rightVotes, reportingPercentage);

                    if (init) ButterKnife.apply(valueBars, INIT_VALUE_BAR);

                    if (init) {
                        for (ValueBar valueBar : valueBars) {
                            valueBar.setVisibility(View.VISIBLE);
                        }
                    }

                    ButterKnife.apply(valueBars, UPDATE_VALUE_BAR, new Pair<>(leftPercentage, rightPercentage));
                    ButterKnife.apply(electionWidget, UPDATE_ELECTION_DATA, data);

                    float validVotes = (float) ((double) responseBody.getResults().get(2).getData().get(0).getRow().get(0));
                    float rejectedVotes = (float) ((double) responseBody.getResults().get(2).getData().get(0).getRow().get(1));
                    float totalPolled = (float) ((double) responseBody.getResults().get(2).getData().get(0).getRow().get(2));
                    float regisElectors = (float) ((double) responseBody.getResults().get(2).getData().get(0).getRow().get(3));

                    float validVotesPercentage = (float) ((double) responseBody.getResults().get(3).getData().get(0).getRow().get(0));
                    float rejectedVotesPercentage = (float) ((double) responseBody.getResults().get(3).getData().get(0).getRow().get(1));
                    float totalPolledPercentage = (float) ((double) responseBody.getResults().get(3).getData().get(0).getRow().get(2));

                    ElectionMetaSummary metaSummary = new ElectionMetaSummary(validVotes, rejectedVotes, totalPolled, regisElectors, validVotesPercentage, rejectedVotesPercentage, totalPolledPercentage);
                    ButterKnife.apply(electionMetaWidget, UPDATE_ELECTION_META_DATA, metaSummary);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                Toast.makeText(MainActivity.this, "Please make sure you are connected to the internet", Toast.LENGTH_SHORT).show();
                emptyTextView.setText("Could not update results");
                emptyImageView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();
        try {
            String jsonData = extras.getString("com.parse.Data");
            Gson gson = new Gson();

            PushMessage message = gson.fromJson(jsonData, PushMessage.class);

            if (message.isShowAlert()) {
                if (message.getTitle() != null && message.getAlert() != null) {
                    QustomDialogBuilder dialog = new QustomDialogBuilder(MainActivity.this)
                            .setTitle(message.getTitle())
                            .setIcon(getResources().getDrawable(R.drawable.ic_dialog_logo));

                    if (message.getAlertLong() != null)
                        dialog.setMessage("\n" + message.getAlertLong() + "\n");
                    else dialog.setMessage("\n" + message.getAlert() + "\n");

                    dialog.show();
                }
            }
        }catch (NullPointerException e) {
            e.printStackTrace();
        }

        updateListViewHeader(false);
        getLoaderManager().restartLoader(0, null, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_switch);
        item.setActionView(R.layout.actionbar_switch);

        final SwitchCompat pushSwitch = (SwitchCompat)item.getActionView().findViewById(R.id.pushSwitch);

        pushSwitch.setChecked(false);
        List<String> subscribedChannels = ParseInstallation.getCurrentInstallation().getList("channels");
        for(String channel : subscribedChannels) {
            if(MyApplication.PARSE_PUSH_CHANNEL.equals(channel)) pushSwitch.setChecked(true);
        }

        pushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pushSwitch.setEnabled(false);
                if(isChecked) {
                    ParsePush.subscribeInBackground(MyApplication.PARSE_PUSH_CHANNEL, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e != null) {
                                e.printStackTrace();
                                pushSwitch.setChecked(false);
                                Toast.makeText(MainActivity.this, "Error: could not enable push notifications", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Push notifications enabled", Toast.LENGTH_SHORT).show();
                            }
                            pushSwitch.setEnabled(true);
                        }
                    });
                } else {
                    ParsePush.unsubscribeInBackground(MyApplication.PARSE_PUSH_CHANNEL, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e != null) {
                                e.printStackTrace();
                                pushSwitch.setChecked(true);
                                Toast.makeText(MainActivity.this, "Error: could not disable push notifications", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Push notifications disabled", Toast.LENGTH_SHORT).show();
                            }
                            pushSwitch.setEnabled(true);
                        }
                    });
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(MainActivity.this, Neo4jProvider.URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
