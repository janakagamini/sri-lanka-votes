package org.slelections.slelections.neo4j;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public class Neo4jAPIManager {

    private static final String API_URL = "http://slelectionsprod2.sb02.stations.graphenedb.com:24789/db/data/";
    private static final RestAdapter REST_ADAPTER = new RestAdapter.Builder()
            .setEndpoint(API_URL)
            .build();
    private static final Neo4jService REST_API = REST_ADAPTER.create(Neo4jService.class);

    public static Neo4jService getRestApi() {
        return REST_API;
    }

    public interface Neo4jService {

        @Headers({
                "Content-Type: application/json",
                "Authorization: Basic c2xlbGVjdGlvbnNfcHJvZDI6NW1DSnFpdVZ4Y01UVWJlRTdlRHM="
        })
        @POST("/transaction/commit")
        void runCypher(@Body RequestBody requestBody, Callback<ResponseBody> cb);

        @Headers({
                "Content-Type: application/json",
                "Authorization: Basic c2xlbGVjdGlvbnNfcHJvZDI6NW1DSnFpdVZ4Y01UVWJlRTdlRHM="
        })
        @POST("/transaction/commit")
        ResponseBody runCypher(@Body RequestBody requestBody);
    }

}
