package org.slelections.slelections.neo4j;

import java.util.List;

public class ResponseBody {
    List<Result> results;
    List<java.lang.Error> errors;

    public List<Result> getResults() {
        return results;
    }

    public List<java.lang.Error> getErrors() {
        return errors;
    }
}
