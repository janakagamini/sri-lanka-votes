package org.slelections.slelections.neo4j;

public class Statement {
    String statement;

    public Statement(String statement) {
        this.statement = statement;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }
}
