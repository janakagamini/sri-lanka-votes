package org.slelections.slelections.neo4j;

import java.util.List;

public class Result {
    List<String> columns;
    List<Row> data;

    public List<String> getColumns() {
        return columns;
    }

    public List<Row> getData() {
        return data;
    }
}
