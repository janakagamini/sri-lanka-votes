package org.slelections.slelections.neo4j;

import java.util.List;

public class RequestBody {
    List<Statement> statements;

    public RequestBody(List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        this.statements = statements;
    }
}
