package org.slelections.slelections.neo4j;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class Neo4jProvider extends ContentProvider {

    private static final String AUTHORITY = "org.slelections.slelections.providers.neo4j";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY);
    private static final int PROVINCE = 10;
    private static final int DISTRICT = 20;
    private static final int DIVISION = 30;
    private static final int CANDIDATE = 40;
    private static final String PROVINCE_PATH = "province";
    public static final Uri PROVINCE_URI = Uri.parse("content://" + AUTHORITY + "/" + PROVINCE_PATH);
    private static final String DISTRICT_PATH = "district";
    public static final Uri DISTRICT_URI = Uri.parse("content://" + AUTHORITY + "/" + DISTRICT_PATH);
    private static final String DIVISION_PATH = "division";
    public static final Uri DIVISION_URI = Uri.parse("content://" + AUTHORITY + "/" + DIVISION_PATH);
    private static final String CANDIDATE_PATH = "candidate";
    public static final Uri CANDIDATE_URI = Uri.parse("content://" + AUTHORITY + "/" + CANDIDATE_PATH);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, PROVINCE_PATH, PROVINCE);
        uriMatcher.addURI(AUTHORITY, DISTRICT_PATH, DISTRICT);
        uriMatcher.addURI(AUTHORITY, DIVISION_PATH, DIVISION);
        uriMatcher.addURI(AUTHORITY, CANDIDATE_PATH, CANDIDATE);
    }

    private static final String[] DIVISION_COLUMNS = {
            "_id",
            "divisionName",
            "districtName",
            "can1_name",
            "can2_name",
            "can1_color",
            "can2_color",
            "can1_vote",
            "can2_vote",
            "can1_p",
            "can2_p",
            "otherVotes",
            "op",
            "timestamp"
    };

    private static final String[] DISTRICT_COLUMNS = {
            "_id",
            "districtName",
            "can1_name",
            "can2_name",
            "can1_color",
            "can2_color",
            "can1_vote",
            "can2_vote",
            "can1_p",
            "can2_p",
            "otherVotes",
            "op",
    };

    Statement cypherResultQuery = new Statement("match (:Election {year:2015})-[:HAS_RESULT]-(result:Result)-[:HAS_RESULT]-(division:Division)-[:BELONGS_TO]-(district:District)" +
            " match (result)-[polled:POLLED]-(candidate:Candidate)" +
            " match (candidate)-[:MEMBER_OF]-(party:Party)" +
            " with party, candidate, result, id(result) as _id, division.name as divisionName, district.name as districtName, polled order by polled.votes desc" +
            " with result, _id, divisionName, districtName, collect(candidate.name) as names, collect(party.color) as colors, collect(polled.votes) as votes, collect(polled.votes/toFloat(result.validVotes)*100) as cp" +
            " with result, _id, divisionName, districtName, names, colors, votes, cp, reduce(otherVotes = result.validVotes, vote in votes| otherVotes - vote) as otherVotes" +
            " with result, _id, divisionName, districtName, names, colors, votes, cp, otherVotes, otherVotes/toFloat(result.validVotes)*100 as op, result.created_at as timestamp" +
            " return _id, divisionName, districtName, names, colors, votes, cp, otherVotes, op, timestamp order by timestamp desc limit 10");

    Statement cypherDistrictQuery = new Statement("match (e:Election) where e.year = 2015 match (e)-[:HAS_RESULT]->(r:Result)<-[:HAS_RESULT]-()-[:BELONGS_TO]->(d:District) match (c:Candidate)-[p:POLLED]-(r) match (c)-[:MEMBER_OF]->(pa:Party) with pa, id(d) as _id, d.name as districtName, c.name as name, sum(p.votes) as votes, sum(r.validVotes) as validVotes order by votes desc with _id, validVotes, districtName, collect(name) as names, collect(pa.color) as colors, collect(votes) as votes, collect(votes/toFloat(validVotes)*100) as perc with _id, validVotes, districtName, names, colors, votes, perc, reduce(otherVotes = validVotes, vote in votes|otherVotes - vote) as ov return _id, districtName, names, colors, votes, perc, ov, ov/toFloat(validVotes)*100 as op order by districtName");

    public Neo4jProvider() {
    }

    @Override
    public boolean onCreate() {
        // TODO: Implement this to initialize your content provider on startup.
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

//        final MatrixCursor nCursor = new MatrixCursor(DIVISION_COLUMNS);
//
//        List<Statement> statements = new ArrayList<>();
//        statements.add(cypherResultQuery);
//
//        RequestBody body = new RequestBody(statements);
//
//        try {
//            ResponseBody responseBody = Neo4jAPIManager.getRestApi().runCypher(body);
//            for (Row row : responseBody.getResults().get(0).getData()) {
//
//                long _id = (long) ((double) row.getRow().get(0));
//                String divisionName = (String) row.getRow().get(1);
//                String districtName = (String) row.getRow().get(2);
//                List<String> names = (List<String>) row.getRow().get(3);
//                List<String> colors = (List<String>) row.getRow().get(4);
//                List<Integer> votes = (List<Integer>) row.getRow().get(5);
//                List<Double> cp = (List<Double>) row.getRow().get(6);
//                double otherVotes = ((double) row.getRow().get(7));
//                double op = (double) row.getRow().get(8);
//                long timeStamp = 0;
//                if (row.getRow().get(9) != null) timeStamp = (long) ((double) row.getRow().get(9));
//
//                nCursor.addRow(new Object[]{_id, divisionName, districtName,
//                        names.get(0), names.get(1),
//                        colors.get(0), colors.get(1),
//                        votes.get(0), votes.get(1),
//                        cp.get(0), cp.get(1),
//                        otherVotes,
//                        op,
//                        timeStamp});
//                nCursor.setNotificationUri(getContext().getContentResolver(), uri);
//            }
//        } catch (RetrofitError error) {
//            error.printStackTrace();
//        }

        final MatrixCursor nCursor = new MatrixCursor(DISTRICT_COLUMNS);

        List<Statement> statements = new ArrayList<>();
        statements.add(cypherDistrictQuery);

        RequestBody body = new RequestBody(statements);

        try {
            ResponseBody responseBody = Neo4jAPIManager.getRestApi().runCypher(body);
            for (Row row : responseBody.getResults().get(0).getData()) {

                long _id = (long) ((double) row.getRow().get(0));
                String districtName = (String) row.getRow().get(1);
                List<String> names = (List<String>) row.getRow().get(2);
                List<String> colors = (List<String>) row.getRow().get(3);
                List<Integer> votes = (List<Integer>) row.getRow().get(4);
                List<Double> perc = (List<Double>) row.getRow().get(5);
                double ov = ((double) row.getRow().get(6));
                double op = (double) row.getRow().get(7);

                nCursor.addRow(new Object[]{_id, districtName,
                        names.get(0), names.get(1),
                        colors.get(0), colors.get(1),
                        votes.get(0), votes.get(1),
                        perc.get(0), perc.get(1),
                        ov,
                        op});
                nCursor.setNotificationUri(getContext().getContentResolver(), uri);
            }

        } catch (RetrofitError error) {
            error.printStackTrace();
        }

        return nCursor;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
