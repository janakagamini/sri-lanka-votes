package org.slelections.slelections;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.philjay.valuebar.ValueBar;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;

public class ResultCursorAdapter extends SimpleCursorAdapter {

    Context context;

    public ResultCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);

        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        super.getView(position, convertView, parent);

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listview_item_division, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.initBars(context);

        if (getCursor().moveToPosition(position)) {

            String divisionName = getCursor().getString(1);
            String districtName = getCursor().getString(2);

            String candidate1 = getCursor().getString(3);
            String candidate2 = getCursor().getString(4);

            int can1_color = Color.parseColor(getCursor().getString(5));
            int can2_color = Color.parseColor(getCursor().getString(6));

            String candidate1_votes = NumberFormat.getNumberInstance(Locale.US).format(getCursor().getInt(7));
            String candidate2_votes = NumberFormat.getNumberInstance(Locale.US).format(getCursor().getInt(8));

            float cp1 = (float) getCursor().getDouble(9);
            float cp2 = (float) getCursor().getDouble(10);
            String candidate1_percentage = String.format("%.2f", cp1) + "%";
            String candidate2_percentage = String.format("%.2f", cp2) + "%";

            String otherVotes = NumberFormat.getNumberInstance(Locale.US).format(getCursor().getInt(11));

            float op = (float) getCursor().getDouble(12);
            String otherPercentage = String.format("%.2f", op) + "%";

            Date date = new Date(getCursor().getLong(13));
            DateFormat format = new SimpleDateFormat("MMM dd`yy 'at' kk:mm 'SLST'");
            format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            String timestamp = format.format(date);


            viewHolder.divisionName.setText(divisionName);
            viewHolder.districtName.setText(districtName + " District");
            viewHolder.candidate1.setText(candidate1.split("(?<=[\\S])[\\S]*\\s*")[0].toUpperCase() + candidate1.split("(?<=[\\S])[\\S]*\\s*")[1].toUpperCase());
            viewHolder.candidate2.setText(candidate2.split("(?<=[\\S])[\\S]*\\s*")[0].toUpperCase() + candidate2.split("(?<=[\\S])[\\S]*\\s*")[1].toUpperCase());
            viewHolder.votes1.setText(candidate1_votes);
            viewHolder.votes2.setText(candidate2_votes);
            viewHolder.cp1.setText(candidate1_percentage);
            viewHolder.cp2.setText(candidate2_percentage);
            viewHolder.other.setText("OTHR");
            viewHolder.otherVotes.setText(otherVotes);
            viewHolder.op.setText(otherPercentage);
            viewHolder.timestamp.setText(timestamp);

            List<Float> results = new ArrayList<>();
            results.add(cp1);
            results.add(cp2);
            results.add(op);

            List<Integer> colors = new ArrayList<>();
            colors.add(can1_color);
            colors.add(can2_color);
            colors.add(context.getResources().getColor(R.color.primary));

            Pair<List<Integer>, List<Float>> pair = new Pair<>(colors, results);
            viewHolder.updateBars(pair);

        }


        return convertView;
    }

    static class ViewHolder {
        final static ButterKnife.Setter<ValueBar, Context> INIT_BARS = new ButterKnife.Setter<ValueBar, Context>() {
            @Override
            public void set(ValueBar bar, Context context, int index) {
                bar.setMinMax(0, 100);
                bar.setValue(0);
                bar.setDrawValueText(false);
                bar.setDrawBorder(false);
                bar.setDrawMinMaxText(false);
                bar.setTouchEnabled(false);
                bar.setBackgroundColor(context.getResources().getColor(R.color.primary_light));
                bar.setColor(context.getResources().getColor(R.color.upfa_blue));
            }
        };
        final static ButterKnife.Setter<ValueBar, Pair<List<Integer>, List<Float>>> UPDATE_BARS = new ButterKnife.Setter<ValueBar, Pair<List<Integer>, List<Float>>>() {
            @Override
            public void set(ValueBar bar, Pair<List<Integer>, List<Float>> pair, int index) {
                bar.setColor(pair.first.get(index));
                bar.animateUp(pair.second.get(index), 500);
            }
        };
        @InjectViews({R.id.candidate1_valueBar, R.id.candidate2_valueBar, R.id.others_valueBar})
        List<ValueBar> bars;

        @InjectView(R.id.divisionName)
        TextView divisionName;

        @InjectView(R.id.districtName)
        TextView districtName;

        @InjectView(R.id.candidate1)
        TextView candidate1;

        @InjectView(R.id.candidate2)
        TextView candidate2;

        @InjectView(R.id.candidate1_votes)
        TextView votes1;

        @InjectView(R.id.candidate2_votes)
        TextView votes2;

        @InjectView(R.id.candidate1_percentage)
        TextView cp1;

        @InjectView(R.id.candidate2_percentage)
        TextView cp2;

        @InjectView(R.id.others_votes)
        TextView otherVotes;

        @InjectView(R.id.others_percentage)
        TextView op;

        @InjectView(R.id.timestamp)
        TextView timestamp;

        @InjectView(R.id.others)
        TextView other;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

        public void initBars(Context context) {
            ButterKnife.apply(bars, INIT_BARS, context);
        }

        public void updateBars(Pair<List<Integer>, List<Float>> pair) {
            ButterKnife.apply(bars, UPDATE_BARS, pair);
        }
    }
}
