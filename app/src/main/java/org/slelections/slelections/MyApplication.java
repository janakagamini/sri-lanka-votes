package org.slelections.slelections;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

public class MyApplication extends Application {

    public static final String PARSE_PUSH_CHANNEL = "resultsProduction";
    //public static final String PARSE_PUSH_CHANNEL = "resultsDebug";

    @Override
    public void onCreate() {
        super.onCreate();

        //PRODUCTION
        Parse.initialize(this, "thURGHHERjowr74jahYktOfueUE3bobq1XAK0ruE", "1AkUErt2AxR8y6wJp2LFMAJFvWJ9o0C16c7GvadY");

        //DEBUG
        //Parse.initialize(this, "L3jtjh1qNIuCskqKR0D2VxRNUF1angTukYLZxIoi", "9TafMfADhaDykxP61G9ojMQLIzhbbO2EkKEwqfdL");

        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParsePush.subscribeInBackground(PARSE_PUSH_CHANNEL);
    }
}
