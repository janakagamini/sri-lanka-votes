package org.slelections.slelections;

public class ElectionSummary {

    float leftPercentage;
    float rightPercentage;
    float leftVotes;
    float rightVotes;
    float reportingPercentage;
    String leftName;
    String rightName;

    public ElectionSummary(String leftName, String rightName, float leftPercentage, float rightPercentage, float leftVotes, float rightVotes, float reporting) {
        this.leftPercentage = leftPercentage;
        this.rightName = rightName;
        this.leftName = leftName;
        this.reportingPercentage = reporting;
        this.rightVotes = rightVotes;
        this.leftVotes = leftVotes;
        this.rightPercentage = rightPercentage;
    }

    public float getLeftPercentage() {
        return leftPercentage;
    }

    public float getRightPercentage() {
        return rightPercentage;
    }

    public float getOthersPercentage() {
        return 100 - (leftPercentage + rightPercentage);
    }

    public float getRightVotes() {
        return rightVotes;
    }

    public float getLeftVotes() {
        return leftVotes;
    }

    public String getLeftName() {
        return leftName;
    }

    public String getRightName() {
        return rightName;
    }

    public float getReportingPercentage() {
        return reportingPercentage;
    }
}
