package org.slelections.slelections;


public class PushMessage {
    String title;
    String alert;
    String alertLong;
    boolean showAlert = false;

    public String getTitle() {
        return title;
    }

    public String getAlert() {
        return alert;
    }

    public String getAlertLong() {
        return alertLong;
    }

    public boolean isShowAlert() {
        return showAlert;
    }
}
