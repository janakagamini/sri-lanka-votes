package org.slelections.slelections;

public class ElectionMetaSummary {
    float validVotes;
    float rejectedVotes;
    float totalPolled;
    float regisElectors;
    float validVotesPercentage;
    float rejectedVotesPercentage;
    float totalPolledPercentage;

    public ElectionMetaSummary(float validVotes, float rejectedVotes, float totalPolled, float regisElectors, float validVotesPercentage, float rejectedVotesPercentage, float totalPolledPercentage) {
        this.validVotes = validVotes;
        this.rejectedVotes = rejectedVotes;
        this.totalPolled = totalPolled;
        this.regisElectors = regisElectors;
        this.validVotesPercentage = validVotesPercentage;
        this.rejectedVotesPercentage = rejectedVotesPercentage;
        this.totalPolledPercentage = totalPolledPercentage;
    }

    public float getValidVotes() {
        return validVotes;
    }

    public float getRejectedVotes() {
        return rejectedVotes;
    }

    public float getTotalPolled() {
        return totalPolled;
    }

    public float getRegisElectors() {
        return regisElectors;
    }

    public float getValidVotesPercentage() {
        return validVotesPercentage;
    }

    public float getRejectedVotesPercentage() {
        return rejectedVotesPercentage;
    }

    public float getTotalPolledPercentage() {
        return totalPolledPercentage;
    }
}
